use r2d2_oracle::OracleConnectionManager;

#[actix_rt::main]
async fn main() {

    let manager =
        OracleConnectionManager::new(
            "entrant_integration",
            "Vz3M@S4stp",
            "(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = oracle.kemsu.ru)(PORT = 1521))(CONNECT_DATA =(SID= ASU)))");

    let pool = r2d2::Pool::builder()
        .max_size(5)
        .build(manager)
        .unwrap();

    let conn = pool.get().unwrap();
    let mut statement = conn.statement("SELECT PROCEDURE_NAME FROM all_procedures WHERE OBJECT_TYPE IN ('FUNCTION','PROCEDURE','PACKAGE') and owner = 'ENTRANT_2022' and object_name = 'SS'")
        .fetch_array_size(30)
        .build()
        .unwrap();
    for row_result in statement.query_as::<String>(&[]).unwrap() {
        let name = row_result.unwrap();
        println!("{}", name);
    }



    println!("Hello, world!");
}
