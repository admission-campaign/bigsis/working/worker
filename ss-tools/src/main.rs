use std::fs::File;
use std::io::Write;
use std::str::FromStr;
use std::thread;
use std::time::Duration;
use reqwest::Client;
use ss_client::auth::SuperShitLogin;
use ss_client::entrants::{Entrant, EntrantListItem};
use ss_client::path::SuperShitUrlBuilder;
use ss_client::errors::ClientResult;
use serde::Serialize;
use ss_client::applications::Application;
use ss_client::applications::dto::NotificationSchema;


#[derive(Serialize, Debug, Clone)]
pub struct NiaisEntrantModel {
    in_gl_mission_id: i32,
    in_gl_year_id: i32,
    in_snils: String,
    in_uidepgu: String,
    in_guidepgu: String,
    in_passport_uidepgu: String,
    in_passport_uid: String,
    in_surname: String,
    in_name: String,
    in_patronymic: String,
    in_IdDocumentType: i32,
    in_passport_date: String,
    in_passport_ser: String,
    in_passport_num: String,
    in_passport_organization: String,
    in_sex: String,
    in_birthday: String,
    in_birthplace: String,
    in_index: String,
    in_id_region: String,
    in_femail: String,
    in_phone: String,
    in_city: String,
    in_city_area: String,
    in_street: String,
    in_house: String,
    in_flat: String,
}


#[derive(Serialize, Debug, Clone)]
pub struct NiaisApplicationParams {
    in_gl_mission_id: i32,
    in_gl_year_id: i32,
    in_emp_guid: String,
    in_uidepgu: String,
    in_UidCompetitiveGroup: String,
    in_AppNumber: String,
    in_RegistrationDate: String,
    in_FirstHigherEducation: String,
    in_NeedHostel: String,
    in_IdStatus: String,
    in_SpecialConditions: String,
    in_Agree: String,
}


#[actix_rt::main]
async fn main() -> ClientResult<()> {
    println!("Hello, world!");
    let url_builder = SuperShitUrlBuilder::new("10.3.60.2", 80u16);

    let ss_login = SuperShitLogin::new("andronovmp@yandex.ru", "Andronov1");
    let session = ss_login.auth(&url_builder).await?
        .select_org(1233, &url_builder).await?;

    println!("Auth complete");

    let needed_cgs = vec![
        "22 5 4949 CG 6543".to_string(), "22 5 4949 CG 6969".to_string(), "22 5 4949 CG 6970".to_string(), "22 5 4949 CG 6547".to_string(), "22 5 4949 CG 6971".to_string(), "22 5 4949 CG 6551".to_string(), "22 5 4949 CG 6973".to_string(), "22 5 4949 CG 6570".to_string(), "22 5 4949 CG 6576".to_string(), "22 5 4949 CG 6974".to_string(), "22 5 4949 CG 6606".to_string(), "22 5 4949 CG 6981".to_string(), "22 5 4949 CG 6983".to_string(), "22 5 4949 CG 6612".to_string(), "22 5 4949 CG 6679".to_string(), "22 5 4949 CG 6987".to_string(), "22 5 4949 CG 6986".to_string(), "22 5 4949 CG 6651".to_string(), "22 5 4949 CG 6996".to_string(), "22 5 4949 CG 6767 del".to_string(), "22 5 4949 CG 6767".to_string(), "22 5 4949 CG 6992".to_string(), "22 5 4949 CG 6647".to_string(), "22 5 4949 CG 6990".to_string(), "22 5 4949 CG 6671".to_string(), "22 5 4949 CG 6995".to_string(), "22 5 4949 CG 6659".to_string(), "22 5 4949 CG 6643".to_string(), "22 5 4949 CG 6985".to_string(), "22 5 4949 CG 6663".to_string(), "22 5 4949 CG 6993".to_string(), "22 5 4949 CG 6989".to_string(), "22 5 4949 CG 6655".to_string(), "22 5 4949 CG 6997".to_string(), "22 5 4949 CG 6621".to_string(), "22 5 4949 CG 6546".to_string(), "22 5 4949 CG 6550".to_string(), "22 5 4949 CG 6554".to_string(), "22 5 4949 CG 6573".to_string(), "22 5 4949 CG 6578".to_string(), "22 5 4949 CG 6608".to_string(), "22 5 4949 CG 6615".to_string(), "22 5 4949 CG 6682".to_string(), "22 5 4949 CG 6654".to_string(), "22 5 4949 CG 6770".to_string(), "22 5 4949 CG 6650".to_string(), "22 5 4949 CG 6674".to_string(), "22 5 4949 CG 6662".to_string(), "22 5 4949 CG 6646".to_string(), "22 5 4949 CG 6666".to_string(), "22 5 4949 CG 6658".to_string(), "22 5 4949 CG 6624".to_string(), "22 5 4949 CG 6545".to_string(), "22 5 4949 CG 6549".to_string(), "22 5 4949 CG 6553".to_string(), "22 5 4949 CG 6572".to_string(), "22 5 4949 CG 6577".to_string(), "22 5 4949 CG 6607".to_string(), "22 5 4949 CG 6614".to_string(), "22 5 4949 CG 6681".to_string(), "22 5 4949 CG 6653".to_string(), "22 5 4949 CG 6769".to_string(), "22 5 4949 CG 6649".to_string(), "22 5 4949 CG 6673".to_string(), "22 5 4949 CG 6661".to_string(), "22 5 4949 CG 6645".to_string(), "22 5 4949 CG 6665".to_string(), "22 5 4949 CG 6657".to_string(), "22 5 4949 CG 6623".to_string()
    ];

    for i in 1..40 {
        let applications_response = session.applications(i, 100).await?;

        println!("{:#?}", applications_response);


        if let Some(applications) = applications_response.data {
            for application in applications {
                if !needed_cgs.contains(&application.uid) {
                    println!("~ skip not needed uid {}", application.uid);
                    continue;
                }

                if application.app_number.contains("22 5 4949") {
                    println!("~ skip not epgu app {}", application.app_number);
                    continue;
                }

                println!("{:#?}", application);
                let application = Application::new(
                    application.id, &*application.entrant_fullname,
                    session.client(),
                    url_builder.clone()
                );
                let response = application.set_status(
                    "in_competition",
                    Some(NotificationSchema {
                        comment: "Публикация конкурсных списков ожидается: ".to_string(),
                        id_notices_types: 10,
                        id_template: None,
                    })
                ).await?;
                println!("{:#?}", response);
                println!("Was set in_competition");

                thread::sleep(Duration::from_secs(1));
            }
        }
    }

    loop {

    }

    let mut entrants: Vec<NiaisEntrantModel> = vec![];
    for i in 1..17 {
        let entrants_response = session.entrants(i, 20).await?;
        println!("{:?}", entrants_response);
        for entrant_list_item in entrants_response.data().into_iter() {
            let entrant = Entrant::new(
                entrant_list_item.birthday(),
                entrant_list_item.created_at(),
                entrant_list_item.id(),
                entrant_list_item.name(),
                entrant_list_item.patronymic(),
                &*entrant_list_item.snils().clone().unwrap_or("".to_string()),
                entrant_list_item.surname(),
                session.client(),
                url_builder.clone(),
            );
            let main_info = entrant.info().await?;
            let identification_doc_schema = entrant.identification_doc().await?;
            let documents = entrant.documents().await?;

            // println!("{:#?}", session.user_info());
            println!(
                "{} {} {} | {}",
                entrant_list_item.surname(),
                entrant_list_item.name(),
                entrant_list_item.patronymic(),
                entrant_list_item.birthday(),
            );

            let niais_entrant = NiaisEntrantModel {
                in_gl_mission_id: 5,
                in_gl_year_id: 22,
                in_snils: entrant_list_item.snils().clone().unwrap_or("".to_string()),
                in_uidepgu: main_info.id.to_string(),
                in_guidepgu: main_info.guid,
                in_passport_uidepgu: identification_doc_schema.uid_epgu.unwrap_or("".to_string()),
                in_passport_uid: identification_doc_schema.id.to_string(),
                in_surname: main_info.surname,
                in_name: main_info.name,
                in_patronymic: main_info.patronymic,
                in_IdDocumentType: identification_doc_schema.id_document_type,
                in_passport_date: identification_doc_schema.issue_date.unwrap_or("".to_string()),
                in_passport_ser: identification_doc_schema.doc_series.unwrap_or("".to_string()),
                in_passport_num: identification_doc_schema.doc_number.unwrap_or("".to_string()),
                in_passport_organization: identification_doc_schema.id_organization.unwrap_or(0).to_string(),
                in_sex: main_info.id_gender.to_string(),
                in_birthday: main_info.birthday,
                in_birthplace: main_info.birthplace,
                in_index: "".to_string(),
                in_id_region: "".to_string(),
                in_femail: main_info.email.unwrap_or("".to_string()),
                in_phone: main_info.phone,
                in_city: "".to_string(),
                in_city_area: "".to_string(),
                in_street: "".to_string(),
                in_house: "".to_string(),
                in_flat: "".to_string(),
            };

            if documents.into_iter().any(|x| {
                if let Some(doc_type) = x.name_document_type {
                    doc_type.eq("Диплом о среднем профессиональном образовании")
                } else {
                    false
                }
            }) {
                entrants.push(niais_entrant);
            }

            //entrants.push(niais_entrant);
            std::thread::sleep(std::time::Duration::from_secs(5));
        }

        let json = serde_json::to_string(&entrants).unwrap();

        let mut file = File::create(format!("fff{}.json", i)).unwrap();
        file.write_all(json.as_bytes()).unwrap();
    }

    Ok(())
}
