pub const CONTENT: &'static str = r#"{
    "математика": [
        "Фомина А. В. – канд. физ.-мат. наук, доцент, декан ФИМЭ, председатель экзаменационной комиссии;",
        "Вячкина Е. А. – канд. физ.-мат. наук, доцент, доцент кафедры МФММ ФИМЭ, старший экзаменатор;",
        "Гаврилова Ю. С. – старший преподаватель кафедры МФММ ФИМЭ, экзаменатор."
    ],
    "информатика и ИКТ": [
        "Маркидонов А.В. – д-р. физ.-мат. наук, зав. кафедрой ИВТ ФИМЭ;",
        "Буяковская И.А. – канд. пед. наук, доцент кафедры ИОТД ФИМЭ;",
        "Густяхина В.П.– старший преподаватель кафедры ИОТД ФИМЭ."
    ],
    "физика": [
        "Тимченко И.И. – канд. пед. наук, доцент, доцент кафедры МФММ ФИМЭ;",
        "Антоненко А.И. – канд. физ.-мат. наук, доцент кафедры МФММ ФИМЭ;",
        "Чмелева К.В. – канд. тех. наук, доцент кафедры геоэкологии и географии ФФКЕП\n."
    ],
    "обществознание": [
        "Борин А.Г. – канд. ист. наук, доцент кафедры истории и обществознания ФИП;",
        "Ершова И.В. – канд. юрид. наук, доцент кафедры государственно-правовых и гражданско-правовых дисциплин ФИП;",
        "Ивлева А.В. – ст. преподаватель кафедры уголовно-правовых дисциплин ФИП;",
        "Музалев А.В. – старший преподаватель кафедры истории и обществознания."
    ],
    "русский язык": [
        "Баланчик Н. С. – канд. филол. наук, доцент, доцент кафедры русского языка и литературы ФФ, председатель экзаменационной комиссии;",
        "Пушкарева И. А. –  д-р филол. наук, доцент профессор кафедры русского языка и литературы ФФ, старший экзаменатор;",
        "Гордеева Л. В. –  канд. пед. наук, доцент кафедры русского языка и литературы ФФ,  экзаменатор."
    ],
    "история": [
        "Борин А. Г. – канд. ист. наук, доцент, доцент кафедры истории и обществознания ФИП;",
        "Макарчева Е. Б. – канд. ист. наук, доцент кафедры истории и обществознания ФИП;",
        "Музалев А.В. – старший преподаватель кафедры истории и обществознания ФИП."
    ],
    "география": [
        "Егорова Н.Т. – канд. пед. наук, доцент, доцент кафедры геоэкологии и географии ФФКЕП;",
        "Ващенко А. Ю.  – канд. геогр. наук, доцент, доцент кафедры геоэкологии и географии ФФКЕП;",
        "Рябов В.А. – канд. геогр. наук, доцент, декан ФФКЕП."
    ],
    "биология": [
        "Горохова Л. Г. – канд. биол. наук, доцент кафедры естественнонаучных дисциплин ФФКЕП;",
        "Баумгертнер М.В. – канд. биол. наук, доцент кафедры педагогики и методики начального образования ФПП;"
    ],
    "английский язык": [
        "Мацуева Т. М. –  канд. пед. наук, доцент,  зав. кафедрой иностранных языков ФФ, председатель экзаменационной комиссии;",
        "Предеина Е. В. – канд. пед. наук, доцент, доцент  кафедры лингвистики ФФ, старший экзаменатор;",
        "Куриленко А. А. – канд. пед. наук, доцент кафедры иностранных языков ФФ, экзаменатор."
    ],
    "испанский язык": [
        "Мацуева Т. М. –  канд. пед. наук, доцент,  зав. кафедрой иностранных языков ФФ, председатель экзаменационной комиссии;",
        "Предеина Е. В. – канд. пед. наук, доцент, доцент  кафедры лингвистики ФФ, старший экзаменатор;",
        "Куриленко А. А. – канд. пед. наук, доцент кафедры иностранных языков ФФ, экзаменатор."
    ],
    "итальянский язык": [
        "Мацуева Т. М. –  канд. пед. наук, доцент,  зав. кафедрой иностранных языков ФФ, председатель экзаменационной комиссии;",
        "Предеина Е. В. – канд. пед. наук, доцент, доцент  кафедры лингвистики ФФ, старший экзаменатор;",
        "Куриленко А. А. – канд. пед. наук, доцент кафедры иностранных языков ФФ, экзаменатор."
    ],
    "китайский язык": [
        "Мацуева Т. М. –  канд. пед. наук, доцент,  зав. кафедрой иностранных языков ФФ, председатель экзаменационной комиссии;",
        "Предеина Е. В. – канд. пед. наук, доцент, доцент  кафедры лингвистики ФФ, старший экзаменатор;",
        "Куриленко А. А. – канд. пед. наук, доцент кафедры иностранных языков ФФ, экзаменатор."
    ],
    "немецкий язык": [
        "Мацуева Т. М. –  канд. пед. наук, доцент,  зав. кафедрой иностранных языков ФФ, председатель экзаменационной комиссии;",
        "Предеина Е. В. – канд. пед. наук, доцент, доцент  кафедры лингвистики ФФ, старший экзаменатор;",
        "Куриленко А. А. – канд. пед. наук, доцент кафедры иностранных языков ФФ, экзаменатор."
    ],
    "французский язык": [
        "Мацуева Т. М. –  канд. пед. наук, доцент,  зав. кафедрой иностранных языков ФФ, председатель экзаменационной комиссии;",
        "Предеина Е. В. – канд. пед. наук, доцент, доцент  кафедры лингвистики ФФ, старший экзаменатор;",
        "Куриленко А. А. – канд. пед. наук, доцент кафедры иностранных языков ФФ, экзаменатор."
    ],
    "профессиональное испытание (общая физическая подготовка)": [
        "Студеникина С. А. – старший преподаватель кафедры физической культуры и спорта ФФКЕП, председатель экзаменационной комиссии;",
        "Зауэр Н. Г. – старший преподаватель кафедры физической культуры и спорта ФФКЕП, старший экзаменатор;",
        "Карпова Т. В. – старший преподаватель кафедры физической культуры и спорта ФФКЕП, экзаменатор;",
        "Артемьев А. А. – канд. пед. наук, доцент, зав. кафедры физической культуры и спорта ФФКЕП, экзаменатор;",
        "Ромашевская Н. И. – канд. пед. наук., доцент кафедры физической культуры и спорта ФФКЕП, экзаменатор."
    ],
    "литература": [
        "Афанасенко О. Б. – канд. пед. наук, доцент кафедры русского языка и литературы ФФ;",
        "Трубицына В.В. – канд. филол. наук, доцент, доцент кафедры русского языка и литературы ФФ;",
        "Гордеева Л.В. – канд. пед. наук, доцент кафедры русского языка и литературы ФФ."
    ],
    "профессиональное испытание (Компьютерный дизайн)": [
        "Можаров М. С. – канд. пед. наук, профессор, зав. кафедрой ИОТД ФИМЭ, председатель экзаменационной комиссии;",
        "Коткин С. Д. – канд. пед. наук, доцент кафедры ИОТД ФИМЭ, старший экзаменатор;",
        "Кравцова О. А. – канд. техн. наук, доцент кафедры ИОТД ФИМЭ, экзаменатор."
    ],
    "профессиональное испытание (профиль УТС)": [
        "Можаров М. С. – канд. пед. наук, профессор, зав. кафедрой ИОТД ФИМЭ, председатель экзаменационной комиссии;",
        "Коткин С. Д. – канд. пед. наук, доцент кафедры ИОТД ФИМЭ, старший экзаменатор;",
        "Кравцова О. А. – канд. техн. наук, доцент кафедры ИОТД ФИМЭ, экзаменатор."
    ],
    "профессиональное испытание (ППДО)": [
        "Чертенкова Г. И. –  канд. пед. наук, доцент кафедры ДиСПП ФПП, председатель экзаменационной комиссии;",
        "Лукьянченко И. В. – канд. пед. наук, доцент кафедры  ДиСПП ФПП, старший экзаменатор;",
        "Попова Л. В. – старший преподаватель кафедры ПМНО ФПП, экзаменатор."
    ],
    "профессиональное испытание (Дошкольная дефектология)": [
        "Чертенкова Г. И. –  канд. пед. наук, доцент кафедры ДиСПП ФПП, председатель экзаменационной комиссии;",
        "Лукьянченко И. В. – канд. пед. наук, доцент кафедры  ДиСПП ФПП, старший экзаменатор;",
        "Попова Л. В. – старший преподаватель кафедры ПМНО ФПП, экзаменатор."
    ],
    "профессиональное испытание (Логопедия)": [
        "Чертенкова Г. И. –  канд. пед. наук, доцент кафедры ДиСПП ФПП, председатель экзаменационной комиссии;",
        "Лукьянченко И. В. – канд. пед. наук, доцент кафедры  ДиСПП ФПП, старший экзаменатор;",
        "Попова Л. В. – старший преподаватель кафедры ПМНО ФПП, экзаменатор."
    ],
    "профессиональное испытание (НОиИ)": [
        "Мартынова А. В. –  канд. пед. наук,  доцент кафедры  ПМНО ФПП, председатель экзаменационной комиссии;",
        "Попова Л. В. – старший преподаватель кафедры ПМНО ФПП, старший экзаменатор;",
        "Махнева О.С. - старший преподаватель кафедры ПМНО ФПП, экзаменатор."
    ],
    "профессиональное испытание (НОиОДД)": [
        "Мартынова А. В. –  канд. пед. наук,  доцент кафедры  ПМНО ФПП, председатель экзаменационной комиссии;",
        "Попова Л. В. – старший преподаватель кафедры ПМНО ФПП, старший экзаменатор;",
        "Махнева О.С. - старший преподаватель кафедры ПМНО ФПП, экзаменатор."
    ],
    "профессиональное испытание (РРиЛ)": [
        "Баланчик Н. С. –  канд. филол. наук, доцент, доцент кафедры русского языка и литературы ФФ, председатель экзаменационной комиссии;",
        "Косточаков Г. В. – канд. филол. наук, доцент кафедры русского языка и литературы ФФ, старший экзаменатор;",
        "Чайковская Е. Н. – канд. филол. наук, доцент кафедры русского языка и литературы ФФ, экзаменатор."
    ],
    "междисциплинарный экзамен магистратура прикладная математика и информатика": [
        "Каледин В. О. – д-р техн. наук, проф., профессор кафедры МФММ ФИМЭ, председатель экзаменационной комиссии;",
        "Решетникова Е. В. – канд. техн. наук, доцент, зав. кафедрой МФММ ФИМЭ, старший экзаменатор;",
        "Вячкина Е. А. – канд. физ.-мат. наук, доцент, доцент кафедры МФММ ФИМЭ, экзаменатор."
    ],
    "междисциплинарный экзамен магистратура экология и природопользование": [
        "Ермак Н. Б.– канд. биол. наук, доцент кафедры геоэкологии и географии, председатель экзаменационной комиссии;",
        "Степанов Ю. А. – д-р техн. наук, доцент, профессор кафедры ИВТ ФИМЭ, старший экзаменатор;",
        "Исакова Е. В. - канд. филос. наук, доцент кафедры  геоэкологии и географии, экзаменатор."
    ],
    "междисциплинарный экзамен магистратура бизнес-информатика": [
        "Степанов Ю. А. – д-р техн. наук, доцент, профессор кафедры ИВТ ФИМЭ, председатель экзаменационной комиссии;",
        "Маркидонов А. В. – д-р. физ.-мат. наук, доцент, доцент кафедры ИВТ, старший экзаменатор;",
        "Бурмин Л. Н. – канд. техн. наук, старший преподаватель кафедры ИВТ, экзаменатор."
    ],
    "междисциплинарный экзамен магистратура социология": [
        "Урбан О. А. – доктор социол. наук, доцент, доцент кафедры ЭУ ФИМЭ, председатель экзаменационной комиссии;",
        "Демчук Н. В. – канд. социол. наук, доцент, доцент кафедры ЭУ ФИМЭ, старший экзаменатор;",
        "Маляр А. А. – старший преподаватель кафедры ЭУ ФИМЭ, экзаменатор."
    ],
    "междисциплинарный экзамен магистратура педагогическое образование (ПиПО)": [
        "Елькина О. Ю. –  д-р пед. наук, проф., заведующий кафедрой ПМНО ФПП, председатель экзаменационной комиссии;",
        "Лозован Л. Я. – канд. пед. наук, доцент, декан ФПП, старший экзаменатор;",
        "Кропочева Т. Б. – д-р. пед. наук., доцент, профессор кафедры  ПМНО ФПП, экзаменатор."
    ],
    "междисциплинарный экзамен магистратура педагогическое образование": [
        "Елькина О. Ю. –  д-р пед. наук, проф., заведующий кафедрой ПМНО ФПП, председатель экзаменационной комиссии;",
        "Лозован Л. Я. – канд. пед. наук, доцент, декан ФПП, старший экзаменатор;",
        "Кропочева Т. Б. – д-р. пед. наук., доцент, профессор кафедры  ПМНО ФПП, экзаменатор."
    ]
}"#;