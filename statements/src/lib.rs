mod exams;
mod context;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate include_dir;

use include_dir::Dir;
use chrono::{Utc, Date};
use std::collections::HashMap;
use std::vec::Vec;
use std::fs::File;
use std::io::BufReader;
use tera::{Tera, Context};
use crate::context::{StatementContext, ExamContext, UserContext, ExaminerContext};
use std::iter::FromIterator;

static TEMPLATES_DIR: Dir = include_dir!("./templates");

lazy_static! {
    static ref TEMPLATES: Tera = {
        TEMPLATES_DIR.extract(".");
        let mut tera = match Tera::new("templates/*.html") {
            Ok(t) => t,
            Err(e) => {
                println!("{}", e);
                std::process::exit(1);
            }
        };
        tera
    };
}

lazy_static! {
    static ref TEMPLATES_META: HashMap<&'static str, Vec<&'static str>> = [
        ("protocol.html", vec!["allow", "approve"]),
        ("statement.html", vec!["exam", "cypher"]),
    ].iter().cloned().collect();
}

lazy_static! {
    static ref EXAMINERS: HashMap<String, Vec<String>> = serde_json::from_str(exams::CONTENT).unwrap();
}

pub async fn generate_statements(date: Date<Utc>) -> bool {
    let session = niais::auth("grigoriev_r", "gHW4MPFs").await
        .select_company(5, 21).await;

    let watchers = session.get_exam_watchers().await.unwrap();
    let exams = session.get_exam_list().await.unwrap();
    let exams: Vec<_> = exams.iter()
        .filter(|(exam_id, exam)| { EXAMINERS.contains_key(exam) })
        .collect();
    for i in 0..exams.len() {
        let (exam_id, exam) = exams[i];
        let users = session.get_exam_users(&date, exam_id).await.unwrap();
        if users.len() > 0 {
            save_statement(
                exam,
                &date,
                &EXAMINERS[exam],
                users,
                watchers.clone(),
            )
        }
    }
    false
}

fn save_statement(
    exam_name: &str,
    date: &Date<Utc>,
    examiners: &Vec<String>,
    users: Vec<(String, String)>,
    watchers: String,
) {
    let users_data: Vec<_> = users.into_iter().enumerate()
        .map(|(idx, (name, score))| {
            UserContext {
                number: (idx + 1) as i32,
                name,
                value: score,
            }
        })
        .collect();

    let examiners_data: Vec<_> = examiners.into_iter().enumerate()
        .map(|(idx, name)| {
            ExaminerContext {
                number: (idx + 1) as i32,
                value: name.to_string(),
            }
        })
        .collect();

    let context = StatementContext {
        type_: "".to_string(),
        watchers,
        examiners: examiners_data,
        exam: ExamContext {
            name: exam_name.to_string(),
            date: date.format("%d-%m-%Y").to_string(),
        },
        users: users_data,
    };

    let word_path = std::path::Path::new("./word");
    let pdf_path = std::path::Path::new("./pdf");

    std::fs::create_dir_all(word_path);
    std::fs::create_dir_all(pdf_path);


    // TEMPLATES_META.iter()
    //     .for_each(|(name, types)| {
    //         types.iter()
    //             .for_each(|type_| {
    //                 let mut context = context.clone();
    //                 context.type_ = type_.to_string();
    //                 let context = Context::from_serialize(context).unwrap();
    //                 let html = TEMPLATES.render(name, &context).unwrap();
    //                 let mut pdf_out = pdf_app.builder()
    //                     .build_from_html(&html)
    //                     .unwrap();
    //                 pdf_out.save("foo.pdf").unwrap();
    //             })
    //     });
}


#[cfg(test)]
mod tests {
    use crate::EXAMINERS;

    #[test]
    fn it_works() {
        println!("123");
        println!("{}", EXAMINERS["математика"][0]);
        assert_eq!(2 + 2, 4);
    }
}
