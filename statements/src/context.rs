
use serde::Serialize;

#[derive(Serialize, Clone)]
pub(crate) struct ExamContext {
    pub(crate) name: String,
    pub(crate) date: String,
}

#[derive(Serialize, Clone)]
pub(crate) struct UserContext {
    pub(crate) number: i32,
    pub(crate) name: String,
    pub(crate) value: String,
}

#[derive(Serialize, Clone)]
pub(crate) struct ExaminerContext {
    pub(crate) number: i32,
    pub(crate) value: String,
}

#[derive(Serialize, Clone)]
pub(crate) struct StatementContext {
    #[serde(rename = "type")]
    pub(crate) type_: String,
    pub(crate) watchers: String,
    pub(crate) examiners: Vec<ExaminerContext>,
    pub(crate) exam: ExamContext,
    pub(crate) users: Vec<UserContext>,
}