use serde::Deserialize;


#[derive(Deserialize, Clone, Debug)]
pub struct Paginator {
    total: i32,
    count_page: i32,
    page: i32,
    offset: i32,
    limit: i32,
}

#[derive(Deserialize, Clone, Debug)]
pub struct Sort {
    sortby: String,
    order: String,
}

#[derive(Debug)]
pub struct ListRequest {
    page: i32,
    limit: i32,
}

impl ListRequest {
    pub fn new(page: i32, limit: i32) -> Self {
        Self { page, limit }
    }
}
