pub mod dto;

use reqwest::{Client, ClientBuilder, Response};
use sha2::{Sha256, Digest};
use crate::auth::dto::{CurrentOrgRequest, CurrentOrgResponse, LoginRequest, LoginResponse};
use crate::{ClientResult, CommonError};
use crate::path::{SuperShitPath, SuperShitUrlBuilder};
use crate::session::SuperShitSession;

#[derive(Debug)]
pub struct SuperShitOrgSelect {
    login_response: LoginResponse,
    client: Client,
}

impl SuperShitOrgSelect {
    pub fn new(login_response: LoginResponse, client: Client) -> Self {
        Self { login_response, client }
    }

    pub async fn select_org(self, org_id: i32, url_builder: &SuperShitUrlBuilder) -> ClientResult<SuperShitSession> {
        let request = CurrentOrgRequest::new(org_id);
        let response = self.client.post(url_builder.build(SuperShitPath::CurrentOrg))
            .json(&request)
            .send()
            .await?;
        let response = response.json::<CurrentOrgResponse>().await?;
        if !response.done() {
            Err(CommonError {
                message: "No Auth".to_string(),
                code: 401,
            })
        } else {
            match self.login_response {
                LoginResponse { organizations_list, user_info, .. } => {
                    Ok(SuperShitSession::new(
                        organizations_list,
                        user_info,
                        self.client.clone(),
                        url_builder.clone()
                    ))
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct SuperShitLogin {
    login: String,
    password: String,
}

impl SuperShitLogin {
    pub fn new(login: &str, password: &str) -> Self {
        let mut hasher = Sha256::new();
        hasher.update(format!("{}{}", login.to_uppercase(), password).as_bytes());
        let buf = hasher.finalize();
        Self { login: login.to_string(), password:  format!("{:x}", &buf) }
    }

    pub async fn auth(self, url_builder: &SuperShitUrlBuilder) -> reqwest::Result<SuperShitOrgSelect> {
        let client = ClientBuilder::new()
            .cookie_store(true)
            .build()?;

        let login_dto = LoginRequest::new(self.login, self.password);
        let response = client.post(url_builder.build(SuperShitPath::Login))
            .json(&login_dto)
            .send()
            .await?;
        let response = client.post(url_builder.build(SuperShitPath::Login))
            .json(&login_dto)
            .send()
            .await?;
        let response = response.json::<LoginResponse>().await?;

        Ok(SuperShitOrgSelect::new(response, client))
    }
}
