use chrono::NaiveDateTime;
use serde::{Serialize, Deserialize};


#[derive(Serialize, Debug)]
pub struct LoginRequest {
    login: String,
    password: String,
}

impl LoginRequest {
    pub fn new(login: String, password: String) -> Self {
        Self { login, password }
    }
}

#[derive(Serialize, Debug)]
pub struct CurrentOrgRequest {
    id: i32
}

impl CurrentOrgRequest {
    pub fn new(id: i32) -> Self {
        Self { id }
    }
}

#[derive(Deserialize, Debug)]
pub struct LoginResponse {
    done: bool,
    message: String,
    pub(crate) organizations_list: Vec<OrganizationSchema>,
    pub(crate) user_info: UserInfoSchema
}

impl LoginResponse {
    pub fn new(
        done: bool,
        message: String,
        organizations_list: Vec<OrganizationSchema>,
        user_info: UserInfoSchema
    ) -> Self {
        Self {
            done,
            message,
            organizations_list,
            user_info
        }
    }
    pub fn done(&self) -> bool {
        self.done
    }
    pub fn message(&self) -> &str {
        &self.message
    }
}

#[derive(Deserialize, Debug)]
pub struct CurrentOrgResponse {
    done: bool
}

impl CurrentOrgResponse {
    pub fn new(done: bool) -> Self {
        Self { done }
    }
    pub fn done(&self) -> bool {
        self.done
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct OrganizationSchema {
    full_title: String,
    id: i32,
    id_eiis: Option<i32>,
    is_oovo: bool,
    kpp: String,
    ogrn: String,
    short_title: String,
    timezone: i32,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RegionSchema {
    Code: Option<String>,
    Created: Option<String>,
    Id: i32,
    Name: String,
    actual: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RoleSchema {
    id: i32,
    actual: bool,
    name: String,
    code: String,
    description: String,
    created: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct UserInfoSchema {
    actual: bool,
    address: Option<String>,
    email: String,
    id: i32,
    id_Author: i32,
    id_region: Option<i32>,
    login: String,
    name: String,
    pass_period: String,
    patronymic: String,
    phone: Option<String>,
    post: Option<String>,
    registration_date: String,
    snils: Option<String>,
    surname: String,
    update_at: Option<String>,
    work: Option<String>,
    region: RegionSchema,
    role: RoleSchema
}