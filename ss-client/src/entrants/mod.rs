mod dto;

use reqwest::Client;
pub use dto::*;
use crate::{ClientResult, CommonError};
use crate::path::{SuperShitPath, SuperShitUrlBuilder};

pub struct Entrant {
    birthday: String,
    created_at: String,
    id: i32,
    name: String,
    patronymic: String,
    snils: String,
    surname: String,
    client: Client,
    url_builder: SuperShitUrlBuilder,
}

impl Entrant {
    pub fn new(
        birthday: &str,
        created_at: &str,
        id: i32,
        name: &str,
        patronymic: &str,
        snils: &str,
        surname: &str,
        client: Client,
        url_builder: SuperShitUrlBuilder,
    ) -> Self {
        Self {
            birthday: birthday.to_string(),
            created_at: created_at.to_string(),
            id,
            name: name.to_string(),
            patronymic: patronymic.to_string(),
            snils: snils.to_string(),
            surname: surname.to_string(),
            client,
            url_builder,
        }
    }

    pub async fn identification_doc(&self) -> ClientResult<EntrantIdentificationDocSchema> {
        let url = self.url_builder.build(SuperShitPath::EntrantIdentificationDocsFile).to_string();
        let url = url.replace("%7B%7D", &*self.id.to_string())
            .replace("%3F", "?");
        let response = self.client.get(url)
            .send()
            .await?;
        let response = response.json::<EntrantIdentificationDocListResponse>().await?;
        if !response.done() {
            Err(CommonError::new(
                &*format!(
                "Entrant id:{} identification not found. Message: {}",
                self.id,
                response.message().clone().unwrap_or_default()
            ), 404))
        } else {
            let identification_doc = response.data.unwrap().get(0).unwrap().docs.get(0).unwrap().clone();
            Ok(identification_doc)
        }
    }

    pub async fn documents(&self) -> ClientResult<Vec<EntrantIdentificationDocSchema>> {
        let url = self.url_builder.build(SuperShitPath::Documents).to_string();
        let url = url.replace("%7B%7D", &*self.id.to_string())
            .replace("%3F", "?");
        let response = self.client.get(url)
            .send()
            .await?;
        let response = response.json::<EntrantIdentificationDocListResponse>().await?;
        if !response.done() {
            Err(CommonError::new(
                &*format!(
                    "Entrant id:{} identification not found. Message: {}",
                    self.id,
                    response.message().clone().unwrap_or_default()
                ), 404))
        } else {
            let identification_doc = response.data.unwrap().get(0).unwrap().docs.clone();
            Ok(identification_doc)
        }
    }

    pub async fn info(&self) -> ClientResult<EntrantMainSchema> {
        let url = self.url_builder.build(SuperShitPath::EntrantMain).to_string();
        let url = url.replace("%7B%7D", &*self.id.to_string());
        let response = self.client.get(url)
            .send()
            .await?;
        let response = response.json::<EntrantMainResponse>().await?;
        if !response.done() {
            // let res = &*response.message().unwrap_or_default();
            Err(CommonError::new(
                &*format!(
                    "Entrant id:{} info not found. Message: {}",
                    self.id,
                    response.message().clone().unwrap_or_default()
                ),
                404
            ))
        } else {
            match response {
                EntrantMainResponse { data, ..} => {
                    if let Some(data) = data {
                        Ok(data)
                    } else {
                        Err(CommonError::new("Entrant id:{} response does not containt data", 444))
                    }
                }
            }
        }
    }
}
