use serde::{Deserialize, Deserializer, Serialize};
use crate::{Paginator, Sort};


#[derive(Deserialize, Clone, Debug)]
pub struct EntrantSearchResponse {
    data: Vec<EntrantListItem>,
    done: bool,
    paginator: Paginator,
    sort: Sort,
}

impl EntrantSearchResponse {
    pub fn data(&self) -> &Vec<EntrantListItem> {
        &self.data
    }
    pub fn done(&self) -> bool {
        self.done
    }
    pub fn paginator(&self) -> &Paginator {
        &self.paginator
    }
    pub fn sort(&self) -> &Sort {
        &self.sort
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct EntrantListItem {
    birthday: String,
    created_at: String,
    id: i32,
    name: String,
    patronymic: String,
    snils: Option<String>,
    surname: String,
}

impl EntrantListItem {
    pub fn birthday(&self) -> &str {
        &self.birthday
    }
    pub fn created_at(&self) -> &str {
        &self.created_at
    }
    pub fn id(&self) -> i32 {
        self.id
    }
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn patronymic(&self) -> &str {
        &self.patronymic
    }
    pub fn snils(&self) -> &Option<String> {
        &self.snils
    }
    pub fn surname(&self) -> &str {
        &self.surname
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct EntrantMainResponse {
    done: bool,
    pub data: Option<EntrantMainSchema>,
    message: Option<String>,
}

impl EntrantMainResponse {
    pub fn done(&self) -> bool {
        self.done
    }
    pub fn data(&self) -> &Option<EntrantMainSchema> {
        &self.data
    }
    pub fn message(&self) -> &Option<String> {
        &self.message
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PhotoSchema {
    id: i32,
    title: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct RegistrationAddressSchema {
    apartament: Option<String>,
    area: Option<String>,
    building1: Option<String>,
    building2: Option<String>,
    city: Option<String>,
    city_area: Option<String>,
    full_addr: Option<String>,
    house: Option<String>,
    id: Option<i32>,
    id_author: Option<i32>,
    id_region: Option<i32>,
    index_addr: Option<String>,
    name_region: Option<String>,
    place: Option<String>,
    street: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EntrantMainSchema {
    pub birthday: String,
    pub birthplace: String,
    pub created: String,
    pub email: Option<String>,
    #[serde(skip)]
    pub fact_address: Option<String>, // fixme
    pub guid: String,
    pub id: i32,
    pub id_gender: i32,
    pub name: String,
    pub name_gender: String,
    pub patronymic: String,
    pub phone: String,
    pub photo: Option<PhotoSchema>,
    pub registration_address: Option<RegistrationAddressSchema>,
    pub snils: Option<String>,
    pub surname: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EntrantIdentificationDocSchema {
    pub can_edit: Option<bool>,
    pub checked: Option<bool>,
    pub doc_number: Option<String>,
    pub doc_series: Option<String>,
    pub has_file: Option<bool>,
    pub id: i32,
    pub id_document_type: i32,
    pub id_entrant: Option<i32>,
    pub id_organization: Option<i32>,
    pub issue_date: Option<String>,
    #[serde(deserialize_with = "str_or_i32")]
    pub mark: Option<String>,
    pub name_check_statuses: Option<String>,
    pub name_document_type: Option<String>,
    pub name_subject: Option<String>,
    pub name_table: Option<String>,
    pub uid_epgu: Option<String>,
}
fn str_or_i32<'de, D>(deserializer: D) -> Result<Option<String>, D::Error>
    where
        D: Deserializer<'de>,
{
    #[derive(Deserialize)]
    #[serde(untagged)]
    enum StrOrU64<'a> {
        Str(&'a str),
        I32(i32),
        Non(Option<()>),
    }

    Ok(match StrOrU64::deserialize(deserializer)? {
        StrOrU64::Str(v) => Some(v.to_string()),
        StrOrU64::I32(v) => Some(v.to_string()),
        StrOrU64::Non(_) => None,
    })
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EntrantIdentificationDocListSchema {
    code_category: Option<String>,
    pub docs: Vec<EntrantIdentificationDocSchema>,
    name_category: Option<String>,
}

#[derive(Deserialize, Clone, Debug)]
pub struct EntrantIdentificationDocListResponse {
    message: Option<String>,
    pub data: Option<Vec<EntrantIdentificationDocListSchema>>,
    done: bool,
}

impl EntrantIdentificationDocListResponse {
    pub fn message(&self) -> &Option<String> {
        &self.message
    }
    pub fn done(&self) -> bool {
        self.done
    }
}

pub struct Address {
    apartment: String,
    area: String,
    building1: String,
}


#[derive(Deserialize, Clone, Debug)]
pub struct CompetitiveListItem {
    actual: bool,
    code: String,
    comment: Option<String>,
    created: String,
    created_at: String,
    id: i32,
    id_author: Option<i32>,
    id_campaign: i32,
    id_comp_org: Option<i32>,
    id_direction: i32,
    id_education_form: i32,
    id_education_level: i32,
    id_education_source: i32,
    id_level_budget: Option<i32>,
    id_organization: i32,
    id_stage: i32,
    name: String,
    name_campaign: String,
    name_direction: String,
    name_education_form: String,
    name_education_level: String,
    name_education_source: String,
    name_level_budget: Option<String>,
    name_organization_okso: String,
    name_stage: String,
    number: i32,
    short_title: Option<String>,
    special_quota: bool,
    uid: String,
    year_end_campaign: i32,
    year_start_campaign: i32,
}


#[derive(Deserialize, Clone, Debug)]
pub struct CompetitiveListResponse {
    message: Option<String>,
    pub data: Option<Vec<CompetitiveListItem>>,
    done: bool,
}
