pub mod auth;
pub mod path;
pub mod session;
pub mod entrants;
pub mod paginator;
pub mod errors;
pub mod applications;

pub(crate) use paginator::*;
pub(crate) use errors::*;

#[cfg(test)]
mod tests {
    use sha2::{Sha256, Digest};
    use crate::auth::dto::LoginResponse;

    use crate::auth::SuperShitLogin;
    use crate::ClientResult;
    use crate::path::SuperShitUrlBuilder;

    #[actix_rt::test]
    async fn it_works() -> ClientResult<()> {
        let result = 2 + 2;
        assert_eq!(result, 4);

        let url_builder = SuperShitUrlBuilder::new("85.142.162.4", 8032u16);

        let ss_login = SuperShitLogin::new("", "");
        let session = ss_login.auth(&url_builder).await?
            .select_org(1233, &url_builder).await?;
        println!("{:#?}", session.user_info());
        println!("{:#?}", session.entrants(1, 100).await?);
        Ok(())
    }

    #[test]
    fn pswd() {
        let login = "andronovmp@yandex.ru".to_string();
        let password = "Andronov1".to_string();
        let mut hasher = Sha256::new();
        hasher.update(format!("{}{}", login.to_uppercase(), password).as_bytes());
        let buf = hasher.finalize();
        println!("{:x}", &buf);
    }

    #[test]
    fn login_resp() {
        let resp = r#"{"user_info":{"id":1256,"login":"andronovmp@yandex.ru","pass_period":"2022-08-16T09:28:36.312401+03:00","patronymic":"Павлович","surname":"Андронов","name":"Максим","id_region":null,"region":{"Id":0,"Code":null,"Name":"","Created":null,"actual":false},"role":{"id":1,"name":"Пользователь","code":"provider","description":"Аня потом поменяет","created":"2020-04-02T11:40:30.26182+03:00","actual":true},"registration_date":"2022-02-16T09:29:18.774845+03:00","post":null,"work":null,"adress":null,"phone":null,"email":"andronovmp@yandex.ru","id_Author":1214,"actual":true,"update_at":null,"snils":null},"organizations_list":[{"full_title":"Кузбасский гуманитарно-педагогический институт федерального государственного бюджетного образовательного учреждения высшего образования \"Кемеровский государственный университет\"","id":1233,"id_eiis":null,"is_oovo":true,"kpp":"421702001","ogrn":"1034205005801","short_title":"КГПИ ФГБОУ ВО \"КемГУ\"","timezone":6}],"done":true,"message":""}"#;
        let login_response: LoginResponse = serde_json::from_str(resp).unwrap();
        println!("{:?}", login_response);
    }
}
