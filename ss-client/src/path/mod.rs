use reqwest::Url;

#[derive(Clone)]
pub struct SuperShitUrlBuilder {
    base_url: Url,
}

impl SuperShitUrlBuilder {
    pub fn new(host: &str, port: u16) -> Self {
        Self {
            base_url: Url::parse(
                &*format!("http://{}:{}", host, port)
            ).unwrap()
        }
    }

    pub fn build(&self, path: SuperShitPath) -> Url {
        let mut url = self.base_url.clone();
        url.set_path(&path.to_string());
        url
    }
}

pub enum SuperShitPath {
    Login,
    CurrentOrg,
    CampaignAdd,
    CampaignList,
    CampaignMain,
    CampaignCheckEditRemove,
    ApplicationSetStatus,
    ApplicationList,
    EntrantList,
    EntrantMain,
    EntrantDocs,
    EntrantIdentificationDocsFile,
    EntrantEducationDocsFile,
    Dictionaries,
    Documents,
}

impl std::string::ToString for SuperShitPath {
    fn to_string(&self) -> String {
        match self {
            Self::Login => "/api/login",
            Self::CurrentOrg => "/api/user/current-org",
            Self::CampaignAdd => "/api/campaign/add",
            Self::CampaignList => "/api/campaign/list",
            Self::CampaignMain => "/api/campaign/{}/main",
            Self::CampaignCheckEditRemove => "/api/campaign/{}/check/edit-and-remove",
            Self::ApplicationSetStatus => "/api/applications/{}/status/set",
            Self::ApplicationList => "/api/applications/list",
            Self::EntrantList => "/api/entrants/list",
            Self::EntrantMain => "/api/entrants/{}/main",
            Self::EntrantDocs => "/api/entrants/{}/docs/short",
            Self::EntrantIdentificationDocsFile => "/api/entrants/{}/docs/short?categories=identification",
            Self::EntrantEducationDocsFile => "/api/docs/general/{}/file",
            Self::Dictionaries => "/api/cls/request",
            Self::Documents => "/api/entrants/{}/docs/short?no_categories=identification",
        }
            .to_string()
    }
}