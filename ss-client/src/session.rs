use reqwest::Client;
use crate::auth::dto::{LoginResponse, OrganizationSchema, UserInfoSchema};
use crate::entrants::{EntrantListItem, EntrantSearchResponse};
use crate::{ClientResult, ListRequest};
use crate::applications::dto::ApplicationListResponse;
use crate::path::{SuperShitPath, SuperShitUrlBuilder};

pub struct SuperShitSession {
    organizations: Vec<OrganizationSchema>,
    user_info: UserInfoSchema,
    client: Client,
    url_builder: SuperShitUrlBuilder,
}

impl SuperShitSession {
    pub fn new(
        organizations: Vec<OrganizationSchema>,
        user_info: UserInfoSchema,
        client: Client,
        url_builder: SuperShitUrlBuilder,
    ) -> Self {
        Self {
            organizations,
            user_info,
            client,
            url_builder,
        }
    }

    pub fn organizations(&self) -> &Vec<OrganizationSchema> {
        &self.organizations
    }

    pub fn user_info(&self) -> &UserInfoSchema {
        &self.user_info
    }

    pub async fn entrants(&self, page: i32, limit: i32) -> ClientResult<EntrantSearchResponse> {
        let query = vec![
            ("page".to_string(), page.to_string()),
            ("limit".to_string(), limit.to_string())
        ];
        let response = self.client.get(self.url_builder.build(SuperShitPath::EntrantList))
            .query(&query)
            .send()
            .await?;
        Ok(response.json::<EntrantSearchResponse>().await?)
    }



    pub async fn applications(&self, page: i32, limit: i32) -> ClientResult<ApplicationListResponse> {
        let query = vec![
            ("page".to_string(), page.to_string()),
            ("limit".to_string(), limit.to_string()),
            ("filter_status".to_string(), 4.to_string()),
        ];
        let response = self.client.get(self.url_builder.build(SuperShitPath::ApplicationList))
            .query(&query)
            .send()
            .await?;
        println!("{}", response.text().await?);
        let response = self.client.get(self.url_builder.build(SuperShitPath::ApplicationList))
            .query(&query)
            .send()
            .await?;
        Ok(response.json::<ApplicationListResponse>().await?)
    }

    pub fn client(&self) -> Client {
        self.client.clone()
    }
}
