use std::fmt::Formatter;
use reqwest::Error;
use serde::Serialize;

#[derive(Serialize, Debug)]
pub struct CommonError {
    pub message: String,
    pub code: i32,
}

impl CommonError {
    pub fn new(message: &str, code: i32) -> Self {
        Self {
            message: message.to_string(),
            code
        }
    }
}

impl From<Error> for CommonError {
    fn from(error: Error) -> Self {
        Self {
            message: error.to_string(),
            code: 1,
        }
    }
}

impl std::fmt::Display for CommonError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error: {}, Code: {}", self.message, self.code)
    }
}

pub type ClientResult<T> = Result<T, CommonError>;
