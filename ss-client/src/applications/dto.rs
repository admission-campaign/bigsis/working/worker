use crate::{Paginator, Sort};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Debug, Clone)]
pub struct ApplicationListRequest {
    page: i32,
    limit: i32,
    filter_status: i32,
}

#[derive(Deserialize, Debug, Clone)]
pub struct ApplicationListResponse {
    done: bool,
    pub data: Option<Vec<ApplicationListResponseItem>>,
    paginator: Paginator,
    sort: Sort,
}

#[derive(Deserialize, Debug, Clone)]
pub struct ApplicationListResponseItem {
    agree: Option<bool>,
    agree_date: Option<String>,
    pub app_number: String,
    changed: Option<String>,
    code_status: Option<String>,
    comment: Option<String>,
    created: Option<String>,
    education_form: Option<String>,
    education_level: Option<String>,
    education_source: Option<String>,
    pub entrant_fullname: String,
    entrant_snils: Option<String>,
    pub id: i32,
    id_campaign: Option<i32>,
    id_competitive_group: Option<i32>,
    id_status: Option<i32>,
    name_status: Option<String>,
    need_hostel: Option<bool>,
    registration_date: Option<String>,
    pub uid: String,
    uid_epgu: Option<i32>,
}

#[derive(Serialize, Debug, Clone)]
pub struct NotificationSchema {
    pub comment: String,
    pub id_notices_types: i32,
    pub id_template: Option<i32>,
}

#[derive(Serialize, Debug, Clone)]
pub struct ApplicationSetStatusRequest {
    pub code: String,
    pub notification: Option<NotificationSchema>,
    pub status_comment: Option<String>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct ApplicationSetStatusResponseData {
    id_application: i32,
    new_status: i32,
}

#[derive(Deserialize, Debug, Clone)]
pub struct ApplicationSetStatusResponse {
    data: Option<ApplicationSetStatusResponseData>,
    done: bool,
    message: Option<String>,
}