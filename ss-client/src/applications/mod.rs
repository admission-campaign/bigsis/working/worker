use reqwest::Client;
use crate::applications::dto::{ApplicationSetStatusRequest, ApplicationSetStatusResponse, NotificationSchema};
use crate::ClientResult;
use crate::path::{SuperShitPath, SuperShitUrlBuilder};

pub mod dto;


pub struct Application {
    id: i32,
    entrant_name: String,
    client: Client,
    url_builder: SuperShitUrlBuilder
}

impl Application {
    pub fn new(
        id: i32,
        entrant_name: &str,
        client: Client,
        url_builder: SuperShitUrlBuilder
    ) -> Self {
        Self {
            id,
            entrant_name: entrant_name.to_string(),
            client,
            url_builder
        }
    }

    pub async fn set_status(&self, status_code: &str, notification: Option<NotificationSchema>) -> ClientResult<ApplicationSetStatusResponse> {
        let url = self.url_builder.build(SuperShitPath::ApplicationSetStatus).to_string();
        let url = url.replace("%7B%7D", &*self.id.to_string())
            .replace("%3F", "?");
        println!("{}", url);
        let response = self.client.post(url)
            .json(&ApplicationSetStatusRequest {
                code: status_code.to_string(),
                notification: notification,
                status_comment: None,
            })
            .send()
            .await?;
        let response = response.json::<ApplicationSetStatusResponse>().await?;
        Ok(response)
    }
}
