use crate::path::{SuperShitPath, SuperShitUrlBuilder};
use serde::{ Serialize, Deserialize };
use crate::dictionaries::TypeString;

pub enum Action {
    Add,
    Remove,
}

impl ToString for Action {
    fn to_string(&self) -> String {
        let result = match self {
            Self::Add => "Add",
            Self::Remove => "Remove",
        };
        result.to_string()
    }
}

pub enum Method {
    DirectionParamsValue,
    OrgDirection,
    Campaign,
    TermsAdmission,
    CampaignAchievement,
    AdmissionVolume,
    DistributedAdmissionVolume,
    CompetitiveGroup,
    EntranceTest,
    EntranceTestLocation,
    ServiceEntrant,
    Identification,
    Document,
    ServiceApplication,
}

impl ToString for Method {
    fn to_string(&self) -> String {
        let result = match self {
            Self::DirectionParamsValue => "DirectionParamsValue",
            Self::OrgDirection => "OrgDirection",
            Self::Campaign => "Campaign",
            Self::TermsAdmission => "TermsAdmission",
            Self::CampaignAchievement => "CampaignAchievement",
            Self::AdmissionVolume => "AdmissionVolume",
            Self::DistributedAdmissionVolume => "DistributedAdmissionVolume",
            Self::CompetitiveGroup => "CompetitiveGroup",
            Self::EntranceTest => "EntranceTest",
            Self::EntranceTestLocation => "EntranceTestLocation",
            Self::ServiceEntrant => "ServiceEntrant",
            Self::Identification => "Identification",
            Self::Document => "Document",
            Self::ServiceApplication => "ServiceApplication",
        };
        result.to_string()
    }
}


pub struct Certificate {
    finger_print: String,
    password: String,
}

impl Certificate {
    pub fn new(finger_print: &str, password: &str) -> Self {
        Self {
            finger_print: finger_print.to_string(),
            password: password.to_string()
        }
    }
}


pub enum DictionaryList {
    CampaignTypeList,
    DocumentTypeVersionList,
}

impl std::string::ToString for DictionaryList {
    fn to_string(&self) -> String {
        let value = match self {
            Self::CampaignTypeList => "CampaignTypeList",
            Self::DocumentTypeVersionList => "DocumentTypeVersionList",
        };
        value.to_string()
    }
}

#[derive(Serialize)]
#[serde(untagged)]
pub enum SyncRequest {
    Dictionary {
        ogrn: String,
        kpp: String,
        cls: String,
    }
}

pub struct Api {
    ignore_certificate: bool,
    certificate: Certificate,
    sync_client: reqwest::blocking::Client,
    async_client: reqwest::Client,
    url_builder: SuperShitUrlBuilder,
}

impl Api {
    pub fn new(
        host: &str,
        port: u16,
        ignore_certificate: bool,
        certificate: Certificate,
    ) -> Self {
        Self {
            ignore_certificate,
            certificate,
            sync_client: reqwest::blocking::Client::new(),
            async_client: reqwest::Client::new(),
            url_builder: SuperShitUrlBuilder::new(host, port),
        }
    }

    pub fn query_dictionary<'a, T: TypeString + Deserialize<'a>>(&self, ogrn: &str, kpp: &str) -> Result<T, ()> {
        let response = self.get_sync(
            SuperShitPath::Dictionaries,
            SyncRequest::Dictionary {
                ogrn: ogrn.to_string(),
                kpp: kpp.to_string(),
                cls: T::type_string(),
            },
        );

        match response {
            Ok(t) => {
                Ok(serde_xml_rs::from_str::<T>(&t).unwrap())
            }
            Err(e) => {
                Err(())
            }
        }
    }

    pub fn get_sync(&self, path: SuperShitPath, request: SyncRequest) -> Result<String, ()> {
        let url = self.url_builder.build(path);
        let result = self.sync_client
            .post(url)
            .json(&request)
            .send();
        match result {
            Ok(response) => {
                Ok(response.text().unwrap())
            }
            Err(err) => {
                println!("{:?}", err);
                Err(())
            }
        }
    }
}
