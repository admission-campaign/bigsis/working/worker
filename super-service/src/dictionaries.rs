use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct CampaignTypeList {
    #[serde(rename = "$value")]
    campaign_types: Vec<CampaignType>,
}

#[derive(Deserialize, Debug)]
pub struct CampaignType {
    Id: i32,
    Actual: bool,
    Name: String,
}

#[derive(Deserialize, Debug)]
pub struct DocumentTypeVersionList {
    #[serde(rename = "$value")]
    document_type_versions: Vec<DocumentTypeVersion>,
}

#[derive(Deserialize, Debug)]
pub struct DocumentTypeVersion {
    Id: i32,
    Description: String,
    IdDocumentType: i32,
    DocVersion: i32,
    FieldsDescription: Option<FieldsDescription>,
}

#[derive(Deserialize, Debug)]
pub struct FieldsDescription {
    #[serde(rename = "$value")]
    fields_description: Vec<FieldDescription>,
}

#[derive(Deserialize, Debug)]
pub struct FieldDescription {
    Name: String,
    Type: String, //todo use enum
    NotNull: bool,
    ClsName: Option<String>,
    Description: String,
}

pub trait TypeString {
    fn type_string() -> String;
}

impl TypeString for DocumentTypeVersionList {
    fn type_string() -> String {
        "DocumentTypeVersionList".to_string()
    }
}

impl TypeString for CampaignTypeList {
    fn type_string() -> String {
        "CampaignTypeList".to_string()
    }
}
