use chrono::NaiveDateTime;
use chrono::NaiveDate;

pub struct CampaignRow {
    UID: String,
    NAME: String,
    YEARSTART: i32,
    YEAREND: i32,
    IDCAMPAIGNSTATUS: i32,
    IDCAMPAIGNTYPE: i32,
    MAXCOUNTACHIEVEMENTS: i32,
    NUMBERAGE: i32,
    COUNTDIRECTIONS: i32,
    ENDDATE: NaiveDateTime,
    EDUCATIONFORMLIST: String,
    EDUCATIONELEVELLIST_ID: String,
}

impl CampaignRow {
    pub fn new_sample() -> Self {
        Self {
            UID: "22 5 4949".to_string(),
            NAME: "Приемная кампания 2022 (КГПИ ФГБОУ ВО КемГУ) - Бакалавриат".to_string(),
            YEARSTART: 2022,
            YEAREND: 2022,
            IDCAMPAIGNSTATUS: 1,
            IDCAMPAIGNTYPE: 1,
            MAXCOUNTACHIEVEMENTS: 10,
            NUMBERAGE: 2,
            COUNTDIRECTIONS: 10,
            ENDDATE: NaiveDate::from_ymd(2022, 10, 31).and_hms(0, 0, 0),
            EDUCATIONFORMLIST: "1,2,3".to_string(),
            EDUCATIONELEVELLIST_ID: "2,3".to_string()
        }
    }
}

pub struct AdmissionVolumeRow {
    Uid: String,
    UidCampaign: String,
    IdEducationLevel: i32,
    IdDirection: i32,
    NumberBudgetO: i32,
    NumberBudgetOZ: i32,
    NumberBudgetZ: i32,
    NumberPaidO: i32,
    NumberPaidOZ: i32,
    NumberPaidZ: i32,
    NumberTargetO: i32,
    NumberTargetOZ: i32,
    NumberTargetZ: i32,
    NumberQuotaO: i32,
    NumberQuotaOZ: i32,
    NumberQuotaZ: i32
}

impl AdmissionVolumeRow {
    pub fn new_sample() -> Self {
        Self {
            Uid: "22 5 4949 AV 2 1316",
            UidCampaign: "22 5 4949",
            IdEducationLevel: 2,
            IdDirection: 1316,
            NumberBudgetO: 0,
            NumberBudgetOZ: 0,
            NumberBudgetZ: 6,
            NumberPaidO: 0,
            NumberPaidOZ: 0,
            NumberPaidZ: 7,
            NumberTargetO: 0,
            NumberTargetOZ: 0,
            NumberTargetZ: 1,
            NumberQuotaO: 0,
            NumberQuotaOZ: 0,
            NumberQuotaZ: 1
        }
    }
}