#[macro_use]
extern crate lazy_static;

mod api;
mod path;
mod dictionaries;
mod oracle;

#[cfg(test)]
mod tests {
    use chrono::{NaiveDate, NaiveDateTime};
    use crate::{api, path};
    use crate::api::{Action, Method};
    use crate::oracle::CampaignRow;


    pub fn sync_campaigns_admission_volume(rows: Vec<CampaignRow>) -> Result<(), ()> {
        Ok(())
    }

    #[test]
    fn it_works() {
        let campaignRow = CampaignRow::new_sample();
        
        let api = api::Api::new(
            "85.142.162.12",
            8031,
            false,
            api::Certificate::new(
                "b5ec2fa09c7716989dde9593b07abb8d5bcb6de0",
                "5ebfntHK",
            ),
        );

        let result = api.query_dictionary::<crate::dictionaries::DocumentTypeVersionList>("1034205005801", "421702001");
        println!("{:#?}", result);


        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
