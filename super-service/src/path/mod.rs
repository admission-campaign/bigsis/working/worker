use reqwest::Url;

pub struct SuperShitUrlBuilder {
    base_url: Url,
}

impl SuperShitUrlBuilder {
    pub fn new(host: &str, port: u16) -> Self {
        Self {
            base_url: Url::parse(
                &*format!("http://{}:{}", host, port)
            ).unwrap()
        }
    }

    pub fn build(&self, path: SuperShitPath) -> Url {
        let mut url = self.base_url.clone();
        url.set_path(&path.to_string());
        url
    }
}

pub enum SuperShitPath {
    Dictionaries,
}

impl std::string::ToString for SuperShitPath {
    fn to_string(&self) -> String {
        match self {
            Self::Dictionaries => "/api/cls/request".to_string()
        }
    }
}
