use crate::session::NiaisUserSession;
use reqwest::{Client, StatusCode};
use crate::path::build_url;

type Result<T> = std::result::Result<T, AuthError>;

#[derive(Debug, Clone)]
pub struct AuthError;


pub struct NiaisUserCredentials {
    login: String,
    password: String,
}

pub struct NiaisUserCompany {
    client: Client,
}

pub async fn auth(login: &str, password: &str) -> NiaisUserCompany {
    NiaisUserCredentials::new(login, password).auth().await.unwrap()
}

impl NiaisUserCredentials {
    pub(crate) fn new(login: &str, password: &str) -> NiaisUserCredentials {
        NiaisUserCredentials { login: login.to_string(), password: password.to_string() }
    }

    pub async fn auth(self) -> Result<NiaisUserCompany> {
        let client = Client::new();
        let path = build_url("index").unwrap();
        let path_str = path.to_string();

        let response = client.post(path)
            .query(&[
                ("href", path_str),
                ("login", self.login.clone()),
                ("password", self.password.clone()),
            ])
            .send().await.unwrap();

        match response.status() {
            StatusCode::OK => {
                let response = response.text().await.unwrap();
                if response.contains("Гость") {
                    Err(AuthError {})
                } else {
                    Ok(NiaisUserCompany::new(client))
                }
            }
            _ => Err(AuthError {})
        }
    }
}

impl NiaisUserCompany {
    fn new(client: Client) -> NiaisUserCompany {
        NiaisUserCompany { client }
    }

    pub async fn select_company(self, mission_id: i8, year_id: i8) -> NiaisUserSession {
        self.client.post(build_url("select_company").unwrap())
            .query(&[
                ("in_mission_id", mission_id.to_string()),
                ("in_year_id", year_id.to_string()),
            ])
            .send().await.unwrap();
        NiaisUserSession::new(mission_id, year_id, self.client)
    }
}