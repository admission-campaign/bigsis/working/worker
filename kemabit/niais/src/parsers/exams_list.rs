use scraper::{Html, Selector, ElementRef};
use std::str::FromStr;

pub(crate) fn parse(html: &str) -> Option<Vec<(u16, String)>> {
    let document = Html::parse_document(html);
    let selector = Selector::parse("#TableSource").unwrap();
    let table = document.select(&selector).next().unwrap();
    let selector = Selector::parse("tr").unwrap();
    Some(
        table.select(&selector)
            .skip(1)
            .map(|element| {
                let selector = Selector::parse("td").unwrap();
                element.select(&selector).collect::<Vec<ElementRef>>()
            })
            .filter(|e| e.len() >= 2)
            .map(|element| {
                (u16::from_str(&*element[1].inner_html()).unwrap(), element[2].inner_html())
            })
            .collect()
    )
}