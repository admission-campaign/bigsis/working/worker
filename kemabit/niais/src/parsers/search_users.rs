use scraper::{Html, Selector, ElementRef};
use std::str::FromStr;

pub(crate) fn parse(html: &str) -> Option<Vec<(String, i32)>> {
    let document = Html::parse_document(html);
    let selector = Selector::parse("body > table > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > div > div > div > table > tbody").unwrap();
    let table = document.select(&selector).next().unwrap();
    let selector = Selector::parse("tr").unwrap();
    Some(
        table.select(&selector)
            .map(|element| {
                let selector = Selector::parse("td").unwrap();
                element.select(&selector).collect::<Vec<ElementRef>>()
            })
            .map(|element| {
                (element[2].inner_html(), i32::from_str(element[10].inner_html().as_str()).unwrap())
            })
            .collect()
    )
}
