use scraper::{Html, Selector, ElementRef};

pub(crate) fn parse(html: &str) -> Option<String> {
    let document = Html::parse_document(html);
    let selector = Selector::parse("#komiss_id").unwrap();
    let elements: Vec<ElementRef> = document.select(&selector).collect();
    if elements.is_empty(){
        None
    } else {
        Some(elements.first().unwrap().inner_html())
    }
}
