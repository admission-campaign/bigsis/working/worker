use scraper::{Html, Selector, ElementRef};
use chrono::{Utc, Date};

pub(crate) fn parse(html: &str, date: &Date<Utc>) -> Option<Vec<(String, String)>> {
    let document = Html::parse_document(html);
    let selector = Selector::parse("#TableSource").unwrap();
    let table = document.select(&selector).next().unwrap();
    let selector = Selector::parse("tr").unwrap();
    let date = date.format("%d-%m-%Y").to_string();
    Some(
        table.select(&selector)
            .skip(3)
            .map(|element| {
                let selector = Selector::parse("td").unwrap();
                element.select(&selector).collect::<Vec<ElementRef>>()
            })
            .filter(|cells| cells.len() >= 8)
            .filter(|cells| cells[5].inner_html().contains(date.as_str()))
            .map(|cells| {
                let selector = Selector::parse("a").unwrap();
                (
                    cells[2].select(&selector).next().unwrap().inner_html(),
                    cells[7].inner_html()
                )
            })
            .collect()
    )
}