use reqwest::Url;
use std::collections::HashMap;

pub(crate) static ENTRANT: &'static str = "entrant_2021";

lazy_static! {

    pub(crate) static ref PATH: HashMap<&'static str, String> = [
        ("index", "/index.htm".to_string()),
        ("auth", "/restricted/index.htm".to_string()),
        ("logout", "/restricted/logoff.htm".to_string()),
        ("select_company", format!("/{}/do_set_var_sess.htm", ENTRANT)),
        ("entrant_index", format!("/{}/index.htm", ENTRANT)),
    ].iter().cloned().collect();
}

pub(crate) fn build_url(name: &str) -> Option<Url> {
    let url = Url::parse("http://niais.kemsu.ru");
    match url {
        Ok(mut url) => {
            url.set_path(&PATH[name]);
            Some(url)
        }
        Err(_) => None
    }
}