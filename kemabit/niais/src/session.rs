use reqwest::{Client, StatusCode};
use crate::path::build_url;
use crate::parsers::{exam_watchers, exams_list, exam_users, search_users};
use chrono::{Date, Utc};

pub struct NiaisUserSession {
    mission_id: i8,
    year_id: i8,
    client: Client,
}

impl NiaisUserSession {
    pub(crate) fn new(mission_id: i8, year_id: i8, client: Client) -> NiaisUserSession {
        NiaisUserSession {
            mission_id,
            year_id,
            client,
        }
    }

    pub async fn get_exam_watchers(&self) -> Option<String> {
        let response =
            self.client.get(build_url("basic_info").unwrap()).send().await.unwrap();

        match response.status() {
            StatusCode::OK => {
                let text = response.text().await.unwrap();
                exam_watchers::parse(&text)
            }
            _ => None
        }
    }

    pub async fn get_exam_list(&self) -> Option<Vec<(u16, String)>> {
        let response = self.client.post(build_url("exam_sess").unwrap())
            .query(&[
                ("commentar", String::from("")),
                ("mission-id", self.mission_id.to_string()),
                ("year-id", self.year_id.to_string()),
                ("session_id", "842".to_string()),
                ("x", "0".to_string()),
                ("y", "0".to_string()),
            ]).send().await.unwrap();

        match response.status() {
            StatusCode::OK => {
                let text = response.text().await.unwrap();
                exams_list::parse(&text)
            },
            _ => None
        }
    }

    pub async fn get_exam_users(&self, date: &Date<Utc>, exam_id: &u16) -> Option<Vec<(String, String)>> {
        let response = self.client.post(build_url("exam_ved").unwrap())
            .query(&[
                ("gl_subject_id", exam_id.to_string()),
                ("x", "0".to_string()),
                ("y", "0".to_string()),
            ]).send().await.unwrap();

        match response.status() {
            StatusCode::OK => {
                let text = response.text().await.unwrap();
                exam_users::parse(&text, date)
            },
            _ => None
        }
    }
}