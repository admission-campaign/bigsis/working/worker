#[macro_use]
extern crate lazy_static;

mod path;
mod auth;
mod session;
mod parsers;

pub use auth::auth;
pub use session::NiaisUserSession;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
