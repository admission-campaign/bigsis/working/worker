use reqwest::Url;
use std::collections::HashMap;

lazy_static! {
    pub(crate) static ref PATH: HashMap<&'static str, &'static str> = [
        ("auth-step-1", "/api/auth/step-1"),
        ("auth-step-2", "/api/auth/step-2"),
        ("getEmpLinkList", "/api/abiturient/operator/getEmpLinkList"),
        ("getEmpPlanList", "/api/abiturient/operator/getEmpPlanList"),
    ].iter().cloned().collect();
}

pub(crate) fn build_url(name: String, private: Option<bool>) -> Option<Url> {
    let private = private.unwrap_or(false);

    let url =
        if private {
            Url::parse("https://abiturient-private-api.kemsu.ru")
        } else {
            Url::parse("https://abiturient-api.kemsu.ru")
        };
    match url {
        Ok(mut url) => {
            url.set_path(PATH[name.as_str()]);
            Some(url)
        }
        Err(_) => None
    }
}