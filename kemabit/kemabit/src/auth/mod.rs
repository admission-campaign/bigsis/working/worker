mod schemas;

use reqwest::{Client, StatusCode};
use std::collections::HashMap;
use crate::path::build_url;
use crate::session::KemUserSession;
use schemas::CodeAuthResponse;

type Result<T> = std::result::Result<T, AuthError>;

#[derive(Debug, Clone)]
pub struct AuthError;

pub fn auth(login: &str, password: &str) -> KemUserCredentials {
    KemUserCredentials::new(login, password)
}

pub struct KemUserCode {
    login: String,
    password: String,
    client: Client,
}

pub struct KemUserCredentials {
    login: String,
    password: String,
}

impl KemUserCredentials {
    pub(crate) fn new(login: &str, password: &str) -> KemUserCredentials {
        KemUserCredentials { login: login.to_string(), password: password.to_string() }
    }

    pub async fn auth_email(self) -> Result<KemUserCode> {
        let client = Client::new();
        let request: HashMap<&str, String> = [
            ("login", self.login.clone()),
            ("password", self.password.clone()),
            ("gatewayTypeId", 2.to_string()),
        ].iter().cloned().collect();
        let response = client.post(
            build_url("auth-step-1".to_string(), Some(false)).unwrap()
        ).json(&request).send().await.unwrap();

        if response.status() == StatusCode::OK {
            Ok(KemUserCode {
                login: self.login.clone(),
                password: self.password.clone(),
                client,
            })
        } else {
            Err(AuthError {})
        }
    }
}

impl KemUserCode {
    pub async fn code(self, code: &str) -> Result<KemUserSession> {
        let request: HashMap<&str, String> = [
            ("login", self.login.clone()),
            ("password", self.password.clone()),
            ("code", code.to_string()),
        ].iter().cloned().collect();

        let response = self.client.post(
            build_url("auth-step-2".to_string(), Some(false)).unwrap()
        ).json(&request).send().await.unwrap();
        if response.status() == StatusCode::OK {
            let access_token =
                response.json::<CodeAuthResponse>().await.unwrap().access_token;
            Ok(KemUserSession::new(&*self.login, self.client, &*access_token))
        } else {
            Err(AuthError {})
        }
    }
}