use serde::Deserialize;

#[derive(Deserialize)]
pub(crate) struct CodeAuthResponse {
    #[serde(rename(deserialize = "accessToken"))]
    pub(crate) access_token: String,
}