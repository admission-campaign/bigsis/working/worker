use reqwest::Client;
use crate::emp_list::schemas::{GetEmpListRequest, GetEmpListResponse, EmpListRow};
use std::collections::HashMap;
use crate::path::build_url;
use reqwest::multipart::Form;
use serde::Serialize;

type Result<T> = std::result::Result<T, RequestError>;

#[derive(Debug, Clone)]
pub struct RequestError;

pub struct KemUserSession {
    login: String,
    client: Client,
    access_token: String,
}

impl KemUserSession {
    pub(crate) fn new(login: &str, client: Client, access_token: &str) -> KemUserSession {
        KemUserSession{
            login: String::from(login),
            client,
            access_token: String::from(access_token),
        }
    }

    fn convert_to_form<T>(request: &T) -> serde_json::error::Result<Form>
    where
        T: ?Sized + Serialize,
    {
        let serialized = serde_json::to_string(&request)?;
        let deserialized: HashMap<String, String> =
            serde_json::from_str(serialized.as_str())?;

        Ok(
            deserialized
            .into_iter()
            .fold(
                reqwest::multipart::Form::new(),
                |form, (k, v)|
                    form.text(k.clone(), v.clone()),
            )
        )
    }


    pub async fn get_emp_list(
        &self,
        request: GetEmpListRequest,
    ) -> Result<Option<Vec<EmpListRow>>> {
        let form = KemUserSession::convert_to_form(&request).unwrap();

        let response =
            self.client.post(
                build_url("getEmpLinkList".to_string(), Some(true)).unwrap()
            )
                .header("x-access-token", self.access_token.clone())
                .multipart(form)
                .send().await;
        match response {
            Ok(response) => {
                let response =
                    response.json::<GetEmpListResponse>().await.unwrap();
                match response {
                    GetEmpListResponse { success: true, .. } => Ok(response.result),
                    _ => Err(RequestError)
                }
            }
            Err(_) => Err(RequestError)
        }
    }
}