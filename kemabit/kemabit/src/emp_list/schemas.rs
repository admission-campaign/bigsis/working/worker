use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use crate::format::date_format;
use crate::format::bool_format::{bool_format, bin_bool_deserialize, option_bool_format};
use std::fmt::Formatter;

#[derive(Serialize)]
pub struct GetEmpListRequest {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fio: Option<String>,
    #[serde(rename = "allFlag")]
    #[serde(serialize_with = "bool_format")]
    pub all_flag: bool,
    #[serde(rename = "olimpFlag")]
    #[serde(serialize_with = "bool_format")]
    pub olimp_flag: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "usersStatusId")]
    pub status: Option<i32>,
    #[serde(rename = "nowayFlag")]
    #[serde(serialize_with = "bool_format")]
    pub noway_flag: bool,
    #[serde(rename = "primaryFlag")]
    #[serde(serialize_with = "bool_format")]
    pub primary_flag: bool,
    #[serde(rename = "targetFlag")]
    #[serde(serialize_with = "bool_format")]
    pub target_flag: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "usersCitizenshipId")]
    pub citizenship: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "activityFlag")]
    #[serde(serialize_with = "option_bool_format")]
    pub activity_flag: Option<bool>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct EmpListRow {
    #[serde(rename = "ADD_DATE")]
    #[serde(with = "date_format")]
    add_date: Option<DateTime<Utc>>,
    #[serde(rename = "CHECK_FLAG")]
    #[serde(deserialize_with = "bin_bool_deserialize")]
    check_flag: bool,
    #[serde(rename = "EMAIL")]
    email: Option<String>,
    #[serde(rename = "EMP_ID")]
    emp_id: i32,
    #[serde(rename = "LOGIN")]
    login: Option<String>,
    #[serde(rename = "NAME")]
    name: Option<String>,
    #[serde(rename = "OPERATOR_LOGIN")]
    operator_login: Option<String>,
    #[serde(rename = "PATRONYMIC")]
    patronymic: Option<String>,
    #[serde(rename = "REG_NUM")]
    reg_num: Option<String>,
    #[serde(rename = "REJECT_FILE_CNT")]
    reject_file_cnt: i32,
    #[serde(rename = "REMARK")]
    remark: Option<String>,
    #[serde(rename = "STATUS")]
    status: Option<String>,
    #[serde(rename = "SUPER_SERVICE_FLAG")]
    #[serde(deserialize_with = "bin_bool_deserialize")]
    super_service_flag: bool,
    #[serde(rename = "SURNAME")]
    surname: Option<String>,
    #[serde(rename = "TARGET_FLAG")]
    #[serde(deserialize_with = "bin_bool_deserialize")]
    target_flag: bool,
}

#[derive(Deserialize)]
pub struct GetEmpListResponse {
    pub result: Option<Vec<EmpListRow>>,
    pub message: Option<String>,
    pub success: bool,
}

impl std::fmt::Display for EmpListRow {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", serde_json::to_string(&self).unwrap())
    }
}