pub mod date_format {
    use chrono::{DateTime, Utc, TimeZone};
    use serde::{self, Deserialize, Serializer, Deserializer};

    const FORMAT: &'static str = "%d-%m-%Y %H:%M:%S";


    pub fn serialize<S>(
        date: &Option<DateTime<Utc>>,
        serializer: S,
    ) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
    {
        match date {
            None => serializer.serialize_none(),
            Some(date) => {
                let s = format!("{}", date.format(FORMAT));
                serializer.serialize_str(&s)
            }
        }
    }

    pub fn deserialize<'de, D>(
        deserializer: D,
    ) -> Result<Option<DateTime<Utc>>, D::Error>
        where
            D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer);
        match s {
            Ok(s) => {
                match Utc.datetime_from_str(&s, FORMAT).map_err(serde::de::Error::custom) {
                    Err(e) => Err(e),
                    Ok(date) => Ok(Some(date))
                }
            }
            Err(_) => Ok(None)
        }
    }
}

pub mod bool_format {
    use serde::{Serializer, Deserializer, Deserialize};

    pub fn bool_format<S>(value: &bool, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer {
        let s = if *value { "1" } else { "0" };
        serializer.serialize_str(s)
    }

    pub fn bin_bool_deserialize<'de, D>(
        deserializer: D,
    ) -> Result<bool, D::Error>
        where
            D: Deserializer<'de>,
    {
        let s = i32::deserialize(deserializer)?;
        match s {
            0 => Ok(false),
            _ => Ok(true)
        }
    }

    pub fn option_bool_format<S>(value: &Option<bool>, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer {
        match *value {
            None => serializer.serialize_none(),
            Some(v) => {
                let s = if v { "1" } else { "0" };
                serializer.serialize_str(s)
            }
        }
    }
}