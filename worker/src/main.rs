extern crate config;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate niais;

use std::io::{Write, Error};

use kemabit::auth::auth;
use kemabit::emp_list::schemas::GetEmpListRequest;
use settings::Settings;
use kemabit::session::KemUserSession;

mod settings;

async fn get_session(login: &str, password: &str) -> std::io::Result<KemUserSession> {
    let auth = auth(login, password)
        .auth_email().await
        .unwrap();
    print!("Enter code: ");
    std::io::stdout().flush();
    let mut input_code = String::new();

    std::io::stdin().read_line(&mut input_code)?;
    Ok(auth.code(input_code.trim()).await.unwrap())
}

#[actix_rt::main]
async fn main() {
    let settings = Settings::new().unwrap();
    let kemabit_settings = settings.kemabit;

    let sess =
        get_session(&*kemabit_settings.login, &*kemabit_settings.password).await.unwrap();
    let list = sess.get_emp_list(GetEmpListRequest {
        fio: None,
        all_flag: true,
        olimp_flag: false,
        status: None,
        noway_flag: false,
        primary_flag: false,
        target_flag: false,
        citizenship: None,
        activity_flag: None,
    }).await.unwrap();
    match list {
        None => {}
        Some(list) => {
            list.into_iter().for_each(|row| println!("{}", row));
        }
    }
}

