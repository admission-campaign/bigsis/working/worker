use config::{ConfigError, Config, File, FileFormat};
use std::path::Path;
use std::env;

type Result<T> = std::result::Result<T, ConfigError>;

#[derive(Debug, Deserialize)]
pub struct Kemabit {
    pub login: String,
    pub password: String,
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub kemabit: Kemabit,
}

impl Settings {
    pub fn new() -> Result<Self> {
        let mut current_path = env::current_exe().unwrap();
        current_path.set_file_name("default.toml");
        let mut config = Config::default();
        config.merge(
            File::from(
                current_path.as_path()
            ))?;

        config.try_into()
    }
}